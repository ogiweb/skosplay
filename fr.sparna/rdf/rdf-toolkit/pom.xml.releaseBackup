<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<parent>
		<groupId>fr.sparna.rdf</groupId>
		<artifactId>sparna-rdf</artifactId>
		<version>1</version>
		<relativePath>../pom.xml</relativePath>
	</parent>

	<groupId>fr.sparna.rdf</groupId>
	<artifactId>rdf-toolkit</artifactId>
	<version>0.30-SNAPSHOT</version>

	<name>Sparna RDF : rdf-toolkit</name>
	<description>Command-line utilities to manipulate RDF data</description>

	<organization>
		<name>Sparna</name>
		<url>http://www.sparna.fr/</url>
	</organization>


	<properties>
		<spring.version>3.0.5.RELEASE</spring.version>
	</properties>

	<build>
		<plugins>

			<!-- Ce plugin ajoute les elements suivants dans le fichier META-INF/MANIFEST.MF 
				du jar final : Implementation-Title: ${project.artifactId} Implementation-Version: 
				${project.version} Implementation-Vendor-Id: ${project.groupId} Cela permet 
				de récupérer la version Maven dans le code, via cette ligne : App.class.getPackage().getImplementationVersion(); 
				L'autre possibilité est d'aller lire le fichier de properties généré automatiquement 
				par Maven à cet endroit : META-INF/maven/${project.groupId}/${project.artifactId}/pom.properties -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<configuration>
					<archive>
						<manifest>
							<addDefaultImplementationEntries>true</addDefaultImplementationEntries>
						</manifest>
					</archive>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

		      <plugin>
			<groupId>org.apache.maven.plugins</groupId>
			<artifactId>maven-shade-plugin</artifactId>
			<version>1.3.2</version>
			<executions>
			  <execution>
			    <phase>package</phase>
			    <goals>
			      <goal>shade</goal>
			    </goals>
			    <configuration>
			      <shadedArtifactAttached>true</shadedArtifactAttached>
			      <shadedClassifierName>onejar</shadedClassifierName>              
			      <transformers>
				<transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
				  <manifestEntries>
				  	  <Main-Class>fr.sparna.rdf.toolkit.Main</Main-Class>
		 			  <Class-Path>.</Class-Path>
		 		  </manifestEntries>
		 		</transformer>
		 		<transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
	      				<resource>META-INF/services/org.openrdf.query.algebra.evaluation.function.Function</resource>
	      			</transformer>
		 		<transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
	      				<resource>META-INF/services/org.openrdf.query.parser.QueryParserFactory</resource>
	      			</transformer>
		 		<transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
	      				<resource>META-INF/services/org.openrdf.query.resultio.BooleanQueryResultParserFactory</resource>
	      			</transformer>
		 		<transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
	      				<resource>META-INF/services/org.openrdf.query.resultio.BooleanQueryResultWriterFactory</resource>
	      			</transformer>
		 		<transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
	      				<resource>META-INF/services/org.openrdf.query.resultio.TupleQueryResultParserFactory</resource>
	      			</transformer>
		 		<transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
	      				<resource>META-INF/services/org.openrdf.query.resultio.TupleQueryResultWriterFactory</resource>
	      			</transformer>
		 		<transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
	      				<resource>META-INF/services/org.openrdf.repository.config.RepositoryFactory</resource>
	      			</transformer>
		 		<transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
	      				<resource>META-INF/services/org.openrdf.rio.RDFParserFactory</resource>
	      			</transformer>
		 		<transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
	      				<resource>META-INF/services/org.openrdf.rio.RDFWriterFactory</resource>
	      			</transformer>
		 		<transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
	      				<resource>META-INF/services/org.openrdf.sail.config.SailFactory</resource>
	      			</transformer>
		 		<transformer implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
	      				<resource>META-INF/services/org.openrdf.sail.rdbms.RdbmsProvider</resource>
	      			</transformer>
			      </transformers>
			    </configuration>
			  </execution>
			</executions>
		      </plugin>

		</plugins>
	</build>

	<dependencies>

	  <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
      </dependency>

		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
		</dependency>

		<dependency> 
		  <groupId>org.slf4j</groupId>
		  <artifactId>slf4j-log4j12</artifactId>
		</dependency>

		<dependency>
			<groupId>org.openrdf.sesame</groupId>
			<artifactId>sesame-runtime</artifactId>
		</dependency>

		<dependency>
			<groupId>fr.sparna.rdf</groupId>
			<artifactId>sesame-toolkit</artifactId>
		</dependency>

		<dependency>
			<groupId>fr.sparna.rdf</groupId>
			<artifactId>skos-toolkit</artifactId>
		</dependency>

		<!-- for command-line parsing -->
		<dependency>
			<groupId>com.beust</groupId>
			<artifactId>jcommander</artifactId>
			<version>1.30</version>
		</dependency>
		
		<!--  Spring dependency -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-core</artifactId>
			<version>${spring.version}</version>
		</dependency>

		<!-- spring-context allows spring configuration file reading -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
			<version>${spring.version}</version>
		</dependency>

	</dependencies>

</project>
