package fr.sparna.rdf.skos.printer.cli.alphabetical;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.openrdf.model.Literal;
import org.openrdf.query.TupleQueryResultHandlerException;
import org.openrdf.repository.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.sparna.commons.xml.fop.FopProvider;
import fr.sparna.rdf.sesame.toolkit.query.Perform;
import fr.sparna.rdf.sesame.toolkit.query.SparqlUpdate;
import fr.sparna.rdf.sesame.toolkit.repository.RepositoryFactoryIfc;
import fr.sparna.rdf.sesame.toolkit.repository.StringRepositoryFactory;
import fr.sparna.rdf.sesame.toolkit.repository.operation.ApplyUpdates;
import fr.sparna.rdf.skos.printer.DisplayPrinter;
import fr.sparna.rdf.skos.printer.cli.SkosPlayCliCommandIfc;
import fr.sparna.rdf.skos.printer.reader.AlphaIndexDisplayGenerator;
import fr.sparna.rdf.skos.printer.reader.BodyReader;
import fr.sparna.rdf.skos.printer.reader.ConceptBlockReader;
import fr.sparna.rdf.skos.printer.reader.HeaderAndFooterReader;
import fr.sparna.rdf.skos.printer.schema.KosDocument;
import fr.sparna.rdf.skos.toolkit.GetLanguagesHelper;
import fr.sparna.rdf.skos.toolkit.SKOSRules;

public class Alphabetical implements SkosPlayCliCommandIfc {

	private Logger log = LoggerFactory.getLogger(this.getClass().getName());

	@Override
	public void execute(Object o) throws Exception {
		// retrieve arguments
		ArgumentsAlphabetical args = (ArgumentsAlphabetical)o;

		// TODO configure logging

		// lire le RDF d'input
		
		RepositoryFactoryIfc factory = new StringRepositoryFactory(args.getInput());
		Repository inputRepository = factory.createNewRepository();

		// SKOS-XL
		ApplyUpdates au = new ApplyUpdates(SparqlUpdate.fromUpdateList(SKOSRules.getSkosXl2SkosRuleset()));
		au.execute(inputRepository);
		
		// build result document
		KosDocument document = new KosDocument();
		
		// build and set header
		HeaderAndFooterReader headerReader = new HeaderAndFooterReader(inputRepository);
		headerReader.setApplicationString("Generated by SKOS Play!, sparna.fr");
		// on désactive complètement le header pour les PDF
		if(args.getFormat() != DisplayPrinter.Format.PDF) {
			// build and set header
			document.setHeader(headerReader.readHeader(
					args.getLang(),
					(args.getConceptScheme() != null)?URI.create(args.getConceptScheme()):null
			));
		}
		// all the time, set footer
		document.setFooter(headerReader.readFooter(
				args.getLang(),
				(args.getConceptScheme() != null)?URI.create(args.getConceptScheme()):null
		));		
		
		ConceptBlockReader cbReader = new ConceptBlockReader(inputRepository);
		cbReader.setSkosPropertiesToRead(AlphaIndexDisplayGenerator.EXPANDED_SKOS_PROPERTIES_WITH_TOP_TERMS);
		cbReader.setStyleAttributes(false);
		// includes multilingual languages if needed
		if(args.isMultilingual()) {
			final String mainLang = args.getLang();
			final List<String> additionalLangs = new ArrayList<String>();
			Perform.on(inputRepository).select(new GetLanguagesHelper() {			
				@Override
				protected void handleLang(Literal lang) throws TupleQueryResultHandlerException {
					if(!lang.stringValue().equals(mainLang) && !lang.stringValue().equals("")) {
						additionalLangs.add(lang.stringValue());
					}
				}
			});
			cbReader.setAdditionalLabelLanguagesToInclude(additionalLangs);
		}
		
		AlphaIndexDisplayGenerator reader = new AlphaIndexDisplayGenerator(inputRepository, cbReader);
		BodyReader bodyReader = new BodyReader(reader);		
		document.setBody(bodyReader.readBody(args.getLang(), (args.getConceptScheme() != null)?URI.create(args.getConceptScheme()):null));

		// if debug needed
		// Marshaller m = JAXBContext.newInstance("fr.sparna.rdf.skos.printer.schema").createMarshaller();
		// m.setProperty("jaxb.formatted.output", true);
		// m.marshal(display, System.out);
		// m.marshal(document, new File("src/main/resources/alpha-index-output-test.xml"));
		
		if(args.getFopConfigPath() != null) {
			log.info("Will use FOP config file path : "+args.getFopConfigPath());
		}

		DisplayPrinter printer = new DisplayPrinter(new FopProvider(args.getFopConfigPath()));
		printer.setStyle(args.getStyle());
		printer.print(document, args.getOutput(), args.getLang(), args.getFormat());
		
		// shutdown repos
		inputRepository.shutDown();

	}

}
